package com.gavrishgerman.dao;

import com.gavrishgerman.entity.PostCodeEntity;
import org.springframework.data.repository.CrudRepository;

public interface PostCodesRepository  extends CrudRepository<PostCodeEntity, Integer> {
    PostCodeEntity findByPostCode(String postCode);
}