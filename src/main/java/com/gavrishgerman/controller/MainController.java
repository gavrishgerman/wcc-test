package com.gavrishgerman.controller;

import com.gavrishgerman.DistanceUtils;
import com.gavrishgerman.dao.PostCodesRepository;
import com.gavrishgerman.entity.PostCodeEntity;
import com.gavrishgerman.entity.PostCodeResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class MainController {
    private Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private PostCodesRepository postCodesRepository;
    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/getDistance", produces = MediaType.APPLICATION_JSON_VALUE)
    public PostCodeResponse getDistance(@RequestParam(value = "origin") String origin,
                                        @RequestParam(value = "destination") String destination) {
        logger.info("origin = " + origin + " destination " + destination);
        PostCodeEntity from = postCodesRepository.findByPostCode(origin);
        PostCodeEntity to = postCodesRepository.findByPostCode(destination);

        double distance = DistanceUtils.calculateDistance(from.getLatitude(), from.getLongitude(),
                to.getLatitude(), to.getLongitude());
        return new PostCodeResponse(from, to, distance);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addNewPostCode(@RequestBody PostCodeEntity postCodeEntity) {
        if (postCodeEntity.isFull()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail, missing properties in request");
        }
        PostCodeEntity byPostCode = postCodesRepository.findByPostCode(postCodeEntity.getPostCode());
        if (byPostCode != null) {
            byPostCode.setLatitude(postCodeEntity.getLatitude());
            byPostCode.setLongitude(postCodeEntity.getLongitude());
            postCodesRepository.save(byPostCode);
        } else {
            postCodesRepository.save(postCodeEntity);
        }
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }

    @RequestMapping(value = "/getActualDistance")
    public PostCodeResponse getActualDistance(@RequestParam(value = "origin") String origin,
                                              @RequestParam(value = "destination") String destination) {
        logger.info("origin = " + origin + " destination " + destination);
        final String baseUrl = "http://maps.googleapis.com/maps/api/directions/json?";

        String url = baseUrl + "sensor=false&mode=driving&language=en&origin=" + origin +
                "&destination=" + destination;
        // Set the Accept header
        HttpEntity<?> requestEntity = new HttpEntity<>(new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
        JSONParser jsonParser = new JSONParser();
        JSONObject parse = null;
        try {
            parse = (JSONObject) jsonParser.parse(response.getBody());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (parse != null) {
            JSONArray routes = (JSONArray) parse.get("routes");
            JSONObject route = (JSONObject) routes.get(0);
            JSONArray legs = (JSONArray) route.get("legs");
            JSONObject leg = (JSONObject) legs.get(0);
            JSONObject distance = (JSONObject) leg.get("distance");
            Double distanceValue = Double.valueOf((Long) distance.get("value"));
            //converting to km
            distanceValue = distanceValue / 1000;

            JSONObject startLocation = (JSONObject) leg.get("start_location");
            Double startLng = (Double) startLocation.get("lng");
            Double startLat = (Double) startLocation.get("lat");

            JSONObject endLocation = (JSONObject) leg.get("end_location");
            Double endLng = (Double) endLocation.get("lng");
            Double endLat = (Double) endLocation.get("lat");

            PostCodeEntity postCodeFrom = new PostCodeEntity(origin, startLat, startLng);
            PostCodeEntity postCodeTo = new PostCodeEntity(destination, endLat, endLng);
            return new PostCodeResponse(postCodeFrom, postCodeTo, distanceValue);
        }
        return null;
    }
}
