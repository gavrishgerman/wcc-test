package com.gavrishgerman.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "ukpostcodes")
@JsonAutoDetect(isGetterVisibility = JsonAutoDetect.Visibility.NONE)
public class PostCodeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @Column(name = "postcode")
    private String postCode;
    @Column
    private Double latitude;
    @Column
    private Double longitude;

    public PostCodeEntity() {
    }

    public PostCodeEntity(String postCode, Double latitude, Double longitude) {
        this.postCode = postCode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public boolean isFull() {
        return latitude == null || longitude == null || postCode == null || postCode.isEmpty();
    }
}
