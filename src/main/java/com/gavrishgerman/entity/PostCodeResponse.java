package com.gavrishgerman.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PostCodeResponse {
    private PostCodeEntity postCodeFrom;
    private PostCodeEntity postCodeTo;
    private double distance;
    private String unit = "km";

    public PostCodeResponse(PostCodeEntity postCodeFrom, PostCodeEntity postCodeTo, double distance) {
        this.postCodeFrom = postCodeFrom;
        this.postCodeTo = postCodeTo;
        this.distance = distance;
    }
}
