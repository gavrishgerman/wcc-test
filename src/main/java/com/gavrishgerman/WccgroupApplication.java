package com.gavrishgerman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WccgroupApplication {

	public static void main(String[] args) {
		SpringApplication.run(WccgroupApplication.class, args);
	}
}
